
#ifndef SL_Watch_Pedo_Wrist_Sleep_Application__H__
#define SL_Watch_Pedo_Wrist_Sleep_Application__H__


extern signed char   SL_SC7A20_PEDO_WRIST_SLEEP_INIT(void);
extern unsigned int  SL_SC7A20_PEDO_WRIST_SLEEP_ALGO(void);


#endif
