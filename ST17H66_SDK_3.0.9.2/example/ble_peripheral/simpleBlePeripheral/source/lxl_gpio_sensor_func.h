#ifndef _LXL_GPIO_SENSOR_FUNC_H_
#define _LXL_GPIO_SENSOR_FUNC_H_

#include "gpio.h"
#include "clock.h"
#include "log.h"

#define MY_GPIO_IIC_SDA				P11
#define MY_GPIO_IIC_SCL				P7

/* ==================== Data Types Declaration Section ==================== */
typedef unsigned char u8;
typedef unsigned short int u16;
typedef unsigned int u32;
typedef unsigned long long u64;
typedef signed char s8;
typedef signed short int s16;
typedef signed int s32;
typedef signed long long s64;

typedef struct {
	u8 dev_time_onoff;       //当前定时的开�?
	u32 dev_at_time; 	     //当前设备的时间戳
	
	u8 dev_sensor_onoff;    //当前sensor的开�?
	u32 dev_steps_data;      //当前设备的步�?
	u8 dev_sensor_data6[6];  //当前sensor的数�?
	
}lxl_iic_func_data;

extern void gsensor_iic_init(void);
extern void iic_readn(u8 chip_id,u8 iic_addr,u8 *iic_dat,u8 n);

#endif

