#ifndef _LXL_GPIO_ADC_COUNT_H_
#define _LXL_GPIO_ADC_COUNT_H_

#include "adc.h"
/**************************************************************************************
* adc检测声明
**************************************************************************************/
/* ==================== Data Types Declaration Section ==================== */
typedef unsigned char u8;
typedef unsigned short int u16;
typedef unsigned int u32;
typedef unsigned long long u64;
typedef signed char s8;
typedef signed short int s16;
typedef signed int s32;
typedef signed long long s64;
//typedef signed int int32;
typedef unsigned short int uint16;
typedef unsigned char uint8;

typedef struct {
    u8 diff_adc_run_flag ;        //当前工作状态://0--idle  1--busy
    u8 adc_echo_flag ;            //当前事件状态://0--busy  1--idle

    u8 adc_enable ;               //adc是否正常工作
    u16 adc_heat_value;           //当前的温度
    u16 adc_ntc_value ;           //当前ntc的温度

    int32 adc_diff_value  ;       //记录当前五次差分数据
}lxl_struct_adc_all ;
extern lxl_struct_adc_all lxl_adc_all_data ;  //adc的状态标志位

extern uint16 lxl_adc_all_func(uint8 task_id, uint16 events) ; //循环处理函数
extern void normal_adc_handle_evt(adc_Evt_t* pev);             //采集adc数据
extern int normal_adc_init(void) ;                             //adc初始化

extern void lxl_gpio_charge_count_func() ;   //充电判断
extern float lxl_get_near_ntc_temp_index(uint32 value) ;
#endif