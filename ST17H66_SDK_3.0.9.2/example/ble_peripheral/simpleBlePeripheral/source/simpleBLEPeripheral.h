/**************************************************************************************************
*******
**************************************************************************************************/

/**************************************************************************************************
  Filename:       simpleBLEperipheral.h
  Revised:         
  Revision:        

  Description:    This file contains the Simple BLE Peripheral sample application
                  definitions and prototypes.

 
**************************************************************************************************/

#ifndef SIMPLEBLEPERIPHERAL_H
#define SIMPLEBLEPERIPHERAL_H

#ifdef __cplusplus
extern "C"
{
#endif

/*********************************************************************
 * INCLUDES
 */

/*********************************************************************
 * CONSTANTS
 */


// Simple BLE Peripheral Task Events
#define SBP_START_DEVICE_EVT					0x0001
#define SBP_RESET_ADV_EVT						0x0002
#define	SBP_DEALDATA							0x0004

#define SBP_SENSOR_GET_CYCLE_TIMER     0x0008
#define GET_STEP_DELAY_TIMER_INTERVAL			500
#define BROCAST_INTERVAL 10*60*2		//10min
#define BROCAST_DURATION 1*60*2		//1min

/*********************************************************************
 * MACROS
 */
#define MAC_DATA_LEN							6
#define STEP_DATA_LEN							11

extern	uint8	dev_mac_data[MAC_DATA_LEN];
extern	uint8	simpleBLEPeripheral_TaskID;
/*********************************************************************
 * FUNCTIONS
 */

/*
 * Task Initialization for the BLE Application
 */
extern void SimpleBLEPeripheral_Init( uint8 task_id );

/*
 * Task Event Processor for the BLE Application
 */
extern uint16 SimpleBLEPeripheral_ProcessEvent( uint8 task_id, uint16 events );

/*********************************************************************
*********************************************************************/

#ifdef __cplusplus
}
#endif

#endif /* SIMPLEBLEPERIPHERAL_H */
