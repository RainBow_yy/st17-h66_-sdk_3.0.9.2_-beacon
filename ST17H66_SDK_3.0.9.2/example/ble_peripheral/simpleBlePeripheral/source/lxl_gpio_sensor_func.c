#include "lxl_gpio_sensor_func.h"

//#define SC7A20_ADDR          0x32
#define SC7A20_ADDR          0x30
#define SC7A20_ADDR_READ     (SC7A20_ADDR+1)

//for test
#define MMA9553_Slave_Addr  0x98 
#define MMA9553_Sub_Addr    0x00

#define SC7A20_OUT_TEMP_L       0x0C
#define SC7A20_OUT_TEMP_H       0x0D
#define SC7A20_WHO_AM_I         0x0F
#define SC7A20_USER_CAL_START   0x13
#define SC7A20_USER_CAL_END     0x1A
#define SC7A20_NVM_WR           0x1E
#define SC7A20_TEMP_CFG         0x1F
#define SC7A20_CTRL_REG1        0x20
#define SC7A20_CTRL_REG2        0x21
#define SC7A20_CTRL_REG3        0x22
#define SC7A20_CTRL_REG4        0x23
#define SC7A20_CTRL_REG5        0x24
#define SC7A20_CTRL_REG6        0x25
#define SC7A20_REFERENCE        0x26
#define SC7A20_STATUS_REG       0x27
#define SC7A20_OUT_X_L          0x28
#define SC7A20_OUT_X_H          0x29
#define SC7A20_OUT_Y_L          0x2A
#define SC7A20_OUT_Y_H          0x2B
#define SC7A20_OUT_Z_L          0x2C
#define SC7A20_OUT_Z_H          0x2D
#define SC7A20_FIFO_CTRL_REG    0x2E
#define SC7A20_SRC_REG          0x2F
#define SC7A20_INT1_CFG         0x30
#define SC7A20_INT1_SOURCE      0x31
#define SC7A20_INT1_THS         0x32
#define SC7A20_INT1_DURATION    0x33
#define SC7A20_INT2_CFG         0x34
#define SC7A20_INT2_SOURCE      0x35
#define SC7A20_INT2_THS         0x36
#define SC7A20_INT2_DURATION    0x37
#define SC7A20_CLICK_CFG        0x38
#define SC7A20_CLICK_SRC        0x39
#define SC7A20_CLICK_THS        0x3A
#define SC7A20_TIME_LIMIT       0x3B
#define SC7A20_TIME_LATENCY     0x3C
#define SC7A20_TIME_WINDOW      0x3D
#define SC7A20_ACT_THS          0x3E
#define SC7A20_ACT_DURATION     0x3F


#define iic_delay()			iic_delay_us(2)

#if 1
lxl_iic_func_data lxl_iic_data_all = {
	.dev_time_onoff=0,       //当前定时的开关
	.dev_at_time=0, 	     //当前设备的时间戳
	
	.dev_sensor_onoff=0,    //当前sensor的开关
	.dev_steps_data=0,      //当前设备的步数
	.dev_sensor_data6={0},  //当前sensor的数据
};   //当前设备的功能标志位



//IIC初始化：
/*----------------------------------------------------------------------------*/
/***@brief   iic_port_init
    @details iic引脚初始化,主要是把clk/data引脚设为输出并打开上拉
    @param   无
    @return  无
    @note    初始化完成后,CLK,DATA均为高
*/
/*----------------------------------------------------------------------------*/

void iic_port_init(void)
{
//    IIC_DATA_DIROUT();               //SDA设置成输出
//    IIC_CLK_DIROUT();                //SCL设置成输出
//    IIC_DATA_HIGH();				 //SDA输出高
//    IIC_CLK_HIGH();					 //SCL输出高
	hal_gpio_fmux(MY_GPIO_IIC_SDA,Bit_ENABLE) ;              //上电设置为gpio口
	hal_gpio_cfg_analog_io(MY_GPIO_IIC_SDA,Bit_DISABLE) ;
	hal_gpio_pin_init(MY_GPIO_IIC_SDA,OEN) ;                  //设置为输入	
	hal_gpio_write(MY_GPIO_IIC_SDA,1) ;                       //对io写高写低

	hal_gpio_fmux(MY_GPIO_IIC_SCL,Bit_ENABLE) ;              //上电设置为gpio口
	hal_gpio_cfg_analog_io(MY_GPIO_IIC_SCL,Bit_DISABLE) ;
	hal_gpio_pin_init(MY_GPIO_IIC_SCL,OEN) ;                  //设置为输入	
	hal_gpio_write(MY_GPIO_IIC_SCL,1) ;                       //对io写高写低
}


void iic_sda_init(u8 flag)
{
	if(flag==0){
		hal_gpio_pin_init(MY_GPIO_IIC_SDA,IE) ;
		hal_gpio_pull_set(MY_GPIO_IIC_SDA, FLOATING) ;
	}else{
		hal_gpio_pin_init(MY_GPIO_IIC_SDA,OEN) ;                  //设置为输入	
		hal_gpio_write(MY_GPIO_IIC_SDA,1) ;                       //对io写高写低
	}
}


void iic_delay_us(u32 us)
{
	WaitUs(us);
}

/*----------------------------------------------------------------------------*/
/***@brief   IIC起始时序：iic_start
    @details IIC 总线启动时序
    @param   无
    @return  无
    @note    产生起始信号后,CLK=0,DATA=0
*/
/*----------------------------------------------------------------------------*/
void iic_start(void)
{
	iic_sda_init(1);
	hal_gpio_write(MY_GPIO_IIC_SDA,1) ; 
	hal_gpio_write(MY_GPIO_IIC_SCL,1) ;
	WaitUs(10);
	hal_gpio_write(MY_GPIO_IIC_SDA,0) ; 
	WaitUs(10);
	hal_gpio_write(MY_GPIO_IIC_SCL,0) ;
//	WaitUs(10);
}

/*----------------------------------------------------------------------------*/
/***@brief   IIC结束时序：iic_stop
    @details IIC 总线停止时序
    @param   无
    @return  无
    @note    结束后,CLK,DATA均为高
*/
/*----------------------------------------------------------------------------*/
void iic_stop(void)
{
	iic_sda_init(1);
	hal_gpio_write(MY_GPIO_IIC_SCL,0) ;
	hal_gpio_write(MY_GPIO_IIC_SDA,0) ; 
	WaitUs(10);
	hal_gpio_write(MY_GPIO_IIC_SCL,1) ;
	WaitUs(10);
	hal_gpio_write(MY_GPIO_IIC_SDA,1) ;
	WaitUs(10);
}

//产生ACK应答
void iic_ack(void)
{
	hal_gpio_write(MY_GPIO_IIC_SCL,0) ;   //拉低时钟线 
	iic_sda_init(1);//IIC_SDA线输出
	hal_gpio_write(MY_GPIO_IIC_SDA,0) ;    //拉低数据线
	WaitUs(10);
	hal_gpio_write(MY_GPIO_IIC_SCL,1) ;   //拉高时钟线
	WaitUs(10);
	hal_gpio_write(MY_GPIO_IIC_SCL,0) ;   //拉低时钟线 
}
//不产生ACK应答		    
void iic_noack(void)
{
  	hal_gpio_write(MY_GPIO_IIC_SCL,0) ;   //拉低时钟线 
	iic_sda_init(1);//IIC_SDA线输出
	hal_gpio_write(MY_GPIO_IIC_SDA,1) ;    //拉高数据线
	WaitUs(10);
	hal_gpio_write(MY_GPIO_IIC_SCL,1) ;   //拉高时钟线
	WaitUs(10);
	hal_gpio_write(MY_GPIO_IIC_SCL,0) ;   //拉低时钟线 

}
/*----------------------------------------------------------------------------*/
/***@brief    IIC应答时序：iic_recv_ack
    @details  等待接受ACK 信号,完成一次操作
    @param    无
    @return   接收到的ack信号,注意ack为0时,才表示有ack信号 //ack为非零,表示没有ack信号
    @note     接收ack之后,CLK为0
*/
/*----------------------------------------------------------------------------*/
u8 iic_recv_ack(void)
{
    u8 ack_flag,m;
	u8 Wait_TOut_Cnt = 0;//设置等待应答信号超时计数
    
	iic_sda_init(0);
	hal_gpio_pull_set(MY_GPIO_IIC_SDA, STRONG_PULL_UP) ;
	WaitUs(10);
	hal_gpio_write(MY_GPIO_IIC_SCL,1) ;
	WaitUs(10);
    ack_flag = hal_gpio_read(MY_GPIO_IIC_SDA);
	while(hal_gpio_read(MY_GPIO_IIC_SDA))
	{
		Wait_TOut_Cnt++;
		if(Wait_TOut_Cnt > 250)
		{
			iic_stop();			 //等待应答信号超时  发送IIC总线停止信号	
			return 0;
		}
	}
	hal_gpio_write(MY_GPIO_IIC_SCL,0) ;
    return ack_flag;
}

/*----------------------------------------------------------------------------*/
/***@brief   IIC写一个BYTE数据接口：iic_send_byte
    @details 模拟iic时序,发送1byte数据
    @param   [in] data 发送的数据
    @return  无
    @note    完成后,clk=0
*/
/*----------------------------------------------------------------------------*/

void iic_send_byte(u8 data)
{
    u8  i,j;
	iic_sda_init(1);
	hal_gpio_write(MY_GPIO_IIC_SCL,0) ;
	WaitUs(10);
    for (i = 0;i<8;i++)
    {
        if (data & 0x80)
        {
            hal_gpio_write(MY_GPIO_IIC_SDA,1) ; //最高位是否为1,为1则SDA= 1,否则 SDA=0
        }else{
            hal_gpio_write(MY_GPIO_IIC_SDA,0) ;
        }
		WaitUs(10);
        hal_gpio_write(MY_GPIO_IIC_SCL,1) ;
      	WaitUs(10);

        data <<= 1;                  			  //数据左移一位,进入下一轮送数
        hal_gpio_write(MY_GPIO_IIC_SCL,0) ;
    }
}


//IIC读取一个字节
//参数值:1		发送Ack
//       0	        不发送Ack
u8 iic_read_byte(u8 SF_Ack)
{
  u8 Rb_Cnt = 0; //读数据位计数 
  u8 RByte  = 0; //读字节
  iic_sda_init(0);//SDA设置为输入
  for(Rb_Cnt=0; Rb_Cnt<8; Rb_Cnt++)
  {
    hal_gpio_write(MY_GPIO_IIC_SCL,0) ; //拉低时钟线   准备开始传送数据位 
    WaitUs(10);
    hal_gpio_write(MY_GPIO_IIC_SCL,1) ; //拉高时钟线
    RByte <<= 1; //数据移位
    if(hal_gpio_read(MY_GPIO_IIC_SDA))
    {
      RByte++;
    }			
    WaitUs(10);
  }					 
  if(!SF_Ack)    //0	不发送Ack
  {
    iic_noack();  //发送NAck
  }
  else           //1		发送Ack
  {
    iic_ack();   //发送Ack
  }		
  return RByte;
}
/*----------------------------------------------------------------------------*/
/***@brief   IIC写一帧数据接口：iic_write_soft
    @details 模拟iic时序,发送一帧数据
    @param   buf 发送的数据包  len发送数据长度
    @return  无
    @note    
*/
/*----------------------------------------------------------------------------*/

void iic_write_soft(u8 *buf,u16 len)
{
	if(len>512)
	{
    	for( u16 i = 0; i < 512; i++ )
	    {
   		 	iic_send_byte(*(buf+i));       //发送数据
   			iic_recv_ack();								//这里没有判断应答信号
   		}
	
		WaitUs(2000);	//延迟10ms等待IIC内部写操作完成
		for( u16 i = 0; i < len-512; i++ )
		{
			iic_send_byte(*(buf+512+i));	 //发送数据
			iic_recv_ack();								 //这里没有判断应答信号
		}
	}else{
		for( u16 i = 0; i < len; i++ )
		{
			iic_send_byte(*(buf+i));      //发送数据
		 	iic_recv_ack();							 //这里没有判断应答信号
   		}
	}
}



u8 iic_revbyte_io( void )
{
    u8 tbyteI2C = 0X00;
    u8 i;
    iic_sda_init(0);
    iic_delay();
    for (i = 0; i < 8; i++)
    {
        hal_gpio_write(MY_GPIO_IIC_SCL,1) ;
        tbyteI2C <<= 1;
        iic_delay();
        if (hal_gpio_read(MY_GPIO_IIC_SDA))
        {
            tbyteI2C++;
        }
        hal_gpio_write(MY_GPIO_IIC_SCL,0) ;
        iic_delay();
    }
    return tbyteI2C;
}

void s_ack(u8 flag)
{
    iic_port_init();
    hal_gpio_write(MY_GPIO_IIC_SCL,0) ;
    if (flag)
    {
        hal_gpio_write(MY_GPIO_IIC_SDA,1) ;
    }
    else
    {
        hal_gpio_write(MY_GPIO_IIC_SDA,0) ;
    }
    iic_delay();
    hal_gpio_write(MY_GPIO_IIC_SCL,1) ;
    iic_delay();
    hal_gpio_write(MY_GPIO_IIC_SCL,0) ;
}

u8 iic_revbyte( u8 para )
{
    u8 tbyte;
    tbyte = iic_revbyte_io();
//    s_ack(para);
    iic_delay();
    return tbyte;
}


void iic_readn(u8 chip_id,u8 iic_addr,u8 *iic_dat,u8 n)
{
	u8 RB_Cnt = 0;
    iic_start();                //I2C启动
    iic_send_byte(chip_id);         //写命令
	iic_recv_ack();
    iic_send_byte(iic_addr);   //写地址
	iic_recv_ack();
    /***+++****/
    iic_start();
    //iic_send_byte(0x33);
    iic_send_byte(SC7A20_ADDR_READ);
	iic_recv_ack();
    /****************/

    for (RB_Cnt=0; RB_Cnt<(n-1); RB_Cnt++)
    {
        iic_dat[RB_Cnt] = iic_read_byte(1);      //读数据
    }
    iic_dat[RB_Cnt] = iic_read_byte(0);
    iic_stop();                 //I2C停止时序
}


void iic_write(u8 chip_id,u8 iic_addr,u8 *iic_dat,u8 n)
{
	u8 WB_Cnt = 0;
    iic_start();                //I2C启动
    iic_send_byte(chip_id);         //写命令
	iic_recv_ack();
    iic_send_byte(iic_addr);   //写地址
	iic_recv_ack();
    for (WB_Cnt=0; WB_Cnt<n; WB_Cnt++)
    {
//        iic_send_byte(*iic_dat++);      //写数据
		iic_send_byte(iic_dat[WB_Cnt]);
		iic_recv_ack();
    }
    iic_stop();                 //I2C停止时序
}


unsigned char SL_MEMS_i2cWrite(unsigned char add, unsigned char reg, unsigned char data)
{
    iic_write(add,reg,&data,1);
    return 0;
}

char SC7A20_Check()
{
	unsigned char reg_value=0;
	iic_readn(SC7A20_ADDR, SC7A20_WHO_AM_I, &reg_value, 1);
	LOG("read chip id reg_value:0x%x\n",reg_value);
	if(reg_value==0x11)
		return 0x01;
	else
		return 0x00;
}


char SC7A20_Config(char ODR,char HP,char click_int,char range,char fifo_en,char fifo_mode,char click_mode,char click_th,char click_window,char click_latency)
{
	unsigned char Check_Flag=0;
	unsigned char write_num=0,i=0;

	Check_Flag=SC7A20_Check();
	LOG("CHECK:%x\n",Check_Flag);
	if(Check_Flag==1)
	{
		LOG("SC7A20_INIT\n");
		
		SL_MEMS_i2cWrite(SC7A20_ADDR, SC7A20_CTRL_REG1, ODR);
		SL_MEMS_i2cWrite(SC7A20_ADDR, SC7A20_CTRL_REG2, HP);
		SL_MEMS_i2cWrite(SC7A20_ADDR, SC7A20_CTRL_REG3, click_int);// click int1
		SL_MEMS_i2cWrite(SC7A20_ADDR, SC7A20_CTRL_REG4, range);
		SL_MEMS_i2cWrite(SC7A20_ADDR, SC7A20_CLICK_THS, fifo_en);
		SL_MEMS_i2cWrite(SC7A20_ADDR, SC7A20_TIME_LIMIT, fifo_mode);
		SL_MEMS_i2cWrite(SC7A20_ADDR, SC7A20_TIME_LATENCY, click_mode);//单Z轴
		SL_MEMS_i2cWrite(SC7A20_ADDR, SC7A20_CLICK_CFG, click_th);//62.6mg(4g)*10

		return 1;
	}
	else
		return 0;
}


//IIC初始化
void gsensor_iic_init(void)
{
	iic_port_init();
//	SC7A20_Config(0x7f,0x0C,0x80,0x90,0x40,0x80,0x15,0x28,0x05,0x10);
//	SC7A20_Config(0x2f,0x04,0xd0,0x00,0x00,0x00,0x4a,0x01,0x15,0x80);
	SC7A20_Config(0x47,0x04,0x80,0x80,0x0A,0x10,0x05,0x15,0x05,0x10);

	u8 temp_reg[8];
//	iic_readn(0x32, 0xa0, temp_reg, 5);
	iic_readn(SC7A20_ADDR, 0x20, &temp_reg[0], 1);
	iic_readn(SC7A20_ADDR, 0x21, &temp_reg[1], 1);
	iic_readn(SC7A20_ADDR, 0x22, &temp_reg[2], 1);
	iic_readn(SC7A20_ADDR, 0x23, &temp_reg[3], 1);
	iic_readn(SC7A20_ADDR, 0x3a, &temp_reg[4], 1);
	iic_readn(SC7A20_ADDR, 0x3b, &temp_reg[5], 1);
	iic_readn(SC7A20_ADDR, 0x3c, &temp_reg[6], 1);
	iic_readn(SC7A20_ADDR, 0x38, &temp_reg[7], 1);
	LOG("======%x %x %x %x %x %x %x %x===========\n",temp_reg[0],temp_reg[1],temp_reg[2],temp_reg[3],temp_reg[4],temp_reg[5],temp_reg[6],temp_reg[7]);
	
}

unsigned char SL_SC7A20_I2c_Spi_Write(unsigned char sl_spi_iic,unsigned char reg, unsigned char dat)
{
	SL_MEMS_i2cWrite(SC7A20_ADDR, reg, dat);
	return 0;
}

unsigned char SL_SC7A20_I2c_Spi_Read(unsigned char sl_spi_iic,unsigned char reg, unsigned char len, unsigned char *buf)
{
	iic_readn(SC7A20_ADDR, reg, buf, len);
	return 0;
}
#endif

